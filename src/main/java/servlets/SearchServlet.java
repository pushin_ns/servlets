package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Product;

@WebServlet("/find")
public class SearchServlet extends HttpServlet {

	Logger logger = LoggerFactory.getLogger(SearchServlet.class);
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nameToFind = request.getParameter("nameToFind");
		List<Product> productList = (List<Product>) getServletContext().getAttribute("products");
		List<Product> searchResults = new ArrayList<>();
		for (Product product : productList) {
			if (product.getName().toLowerCase().contains(nameToFind.toLowerCase()))
				searchResults.add(product);
		}
		HttpSession session = request.getSession();
		session.setAttribute("searchResults", searchResults);
	}
}
