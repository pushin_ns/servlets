package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Product;

@WebServlet("/toCart")
public class AddToCartServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Logger logger = LoggerFactory.getLogger(AddToCartServlet.class);

	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int productId = Integer.parseInt(request.getParameter("id"));
		logger.info("Product ID={} added to cart", productId);
		List<Product> productList = (List<Product>) getServletContext().getAttribute("products");
		HttpSession session = request.getSession();
		List<Product> cart;
		if (session.getAttribute("cart") != null)
			cart = (List<Product>) session.getAttribute("cart");
		else
			cart = new ArrayList<>();
		cart.add(productList.get(productId));
		session.setAttribute("cart", cart);
		session.setAttribute("numberOfItems", cart.size());

	}
}