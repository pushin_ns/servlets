package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private User user;
	Logger logger = LoggerFactory.getLogger(LoginServlet.class);

	@Override
	public void init() {
		user = new User();
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("login") != null)
			request.getRequestDispatcher("logout").forward(request, response);
		else
			request.getRequestDispatcher("/jsp/login.jsp").forward(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String userLogin = user.getLogin();
		if (request.getParameter("login").equals(userLogin)
				&& request.getParameter("password").equals(user.getPassword())) {
			HttpSession session = request.getSession();
			session.setAttribute("login", userLogin);
			logger.info("Set attribute login={} to session", userLogin);
			session.setAttribute("discount", Math.round((Math.random() * 50)));
			response.sendRedirect("index");
		} else {
			response.sendRedirect("login");
		}
	}
}