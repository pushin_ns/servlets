package servlets;


import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Product;

@WebServlet("/deleteFromCart")
public class DeleteFromCartServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Logger logger = LoggerFactory.getLogger(DeleteFromCartServlet.class);

	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		HttpSession session = request.getSession();
		List<Product> cart = (List<Product>) session.getAttribute("cart");
		for (Iterator<Product> iterator = cart.iterator(); iterator.hasNext();) {
			Product product = (Product) iterator.next();
			if (product.getId() == id) {
				iterator.remove();
				break;
			} 
		}	
		session.setAttribute("cart", cart);
		session.setAttribute("numberOfItems", cart.size());

	}
}