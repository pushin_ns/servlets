package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Product;
import beans.User;

import util.Producer;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {

	private User user;
	private static final long serialVersionUID = 1L;
	Logger logger = LoggerFactory.getLogger(IndexServlet.class);

	public void init() {
		user = new User();
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Producer producer = new Producer();
		List<Product> productList = producer.productList;
		HttpSession session = request.getSession();
		session.setAttribute("list", producer.productList);
		getServletContext().setAttribute("products", productList);
		request.getRequestDispatcher("/jsp/index.jsp").forward(request, response);
	}
}