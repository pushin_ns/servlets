package beans;

public class Product {

	private int id;
	private String name;
	private double price;
	private String info;
	private String imageUrl;

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", price=" + price + ", info=" + info + ", imageUrl=" + imageUrl
				+ "]";
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Product(int id, String name, double price, String info, String imageUrl) {
		super();
		this.id = id;
		this.imageUrl = imageUrl;
		this.name = name;
		this.price = price;
		this.info = info;
	}

}
