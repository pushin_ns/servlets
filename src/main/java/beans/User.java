package beans;

public class User {
	private String firstName;
	private String lastName;
	private String login;
	private String password;

	public User() {
		this.firstName = "Nikita";
		this.login = "pushinn";
		this.password = "123456";
		this.lastName = "Pushin";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
