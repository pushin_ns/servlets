$(window).scroll(function () {
    $("h1").each(function () {
        var position = $(this).offset().top;
        var st = $(window).scrollTop();
        if (st % 5 === 0)
            if (st <= 10) {
                $("#sticky-header").removeClass('fixed-header');
                $("#menu").css("border-bottom", "3px solid #666");
                $("#sticky-header").css({ "display": "none", "top": "0" });
            }
            else {
                $("#sticky-header").addClass('fixed-header');
                $("#sticky-header").css("display", "block");
                $("#menu").css("border", "none");
                $("#sticky-header").animate({ top: "49" }, 500);
                if (st > position-20) $("#sticky-header").html($(this).text());
            }
    })

});
