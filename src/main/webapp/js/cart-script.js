$(document).ready(function () {

    $('#cart').click(function (e) {
        e.preventDefault();
        $('#close-cart').click(function (e) {
            hide(e);
        });
        $("#cart-container span:first-child").remove();

        //Lift window to top of document
        var body = $("html, body");
        body.stop().animate({ scrollTop: 0 }, 500, 'swing');
        //Show popup container
        $("#cart-popup-container").animate({ "width": "toggle" }, 100, function drawPopup() {
            $(this).css('height', $(document).height());

            /*Indicates if cart is empty*/
            var flag = 0;
            if (($("#cart-container").html()).replace(/[\t\n]+/g,"") == "<ul></ul>") {
                flag = 1;
                $("#cart-popup").children().each(function () {
                    if ($(this).prop("tagName") != 'H1')
                        $(this).hide();
                });
                $("#cart-container span:first-child").empty();
                $("#cart-container").prepend("<span>В корзине ничего нет</span>").show();

            }
//            Show popup window. If flag equals 0, it will display the list of selected items
            $("#cart-popup").animate({ "height": "toggle", "opacity": 1 }, 0, function () {
                $(this).css("display", "block");
                if (flag === 0) {
                    $("#cart-popup").children().each(function () {
                        $(this).show();
                    });
                    $("#cart-container .price").show();
                    
                    	var sum = Number(0);
                        $("#cart-container li").each(function () {
                            sum += parseInt($(this).find(".price").text());
                            $(this).append("<div class = 'delete-from-cart' title='Удалить из корзины'></div>");
                        });
                        $("#sum").html(sum).append(" руб.");
                        var discount = parseInt($("#discount").html()) / 100;
                        var finalSum = sum - sum * discount;
                        $("#final-sum").html(Math.round(finalSum * 100) / 100).append(" руб.");
                    
                    $('.delete-from-cart').click(function (e) {
                    	var item = $(this).parent();
                    	var id = item.find(".id").html();
                    	$.ajax({
                            type: 'get',
                            url: 'deleteFromCart',
                            data: 'id='+id,
                            success: function(){
                            	$("#cart-indicator").load(document.location.href + " #cart-indicator p");
                            	item.remove();
                            	drawPopup();
                            }
                        });
                    });
                }
            });
            
        });

    });

    $('#cart-popup').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('#cart-popup-container').click(function (e) {
        hide(e);
    });
    
});



function hide(e) {
    $('html').css('overflow', 'auto');
    e.preventDefault();
    $("#cart-popup").hide(300);
    $("#cart-popup-container").hide(100);
    $("#cart-popup").css({ "opacity": "0" });
}
//Popup can't be placed outside of visible area
$(document).ready(function () {
    $('#cart-popup-container').on(
        'mousewheel', function (e) {
            if (e.originalEvent.wheelDelta / 120 > 0)
                return;
            if ($("#performBtn").offset().top > $(window).scrollTop() + 700) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
        }
    )
});