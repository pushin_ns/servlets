$(document).ready(function () {
    $('#search').click(function (e) {
        e.preventDefault();
        $("#search-popup-container").animate({ "width": "toggle" }, 200, function () {
            $(this).css("display", "block");
            $(this).css('height', $(document).height());
            $("#search-popup").animate({ "height": "toggle", "opacity": 2 }, 200, function () {
                $(this).css("display", "block");
            });
            $('#close-search').click(function (e) {
                hide(e);
            });
        });

    });
    $('#search-popup').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
    });
    $('#searchBtn').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = $(this).parent().find("#nameToFind").val();
        $.ajax({
            type: 'get',
            url: 'find',
            data: 'nameToFind=' + data,
            success: function(){
            	$("#search-results").load(document.location.href + " #search-results ul");
            }
        });
    });
    $('#search-popup-container').click(function (e) {
        hide(e);
    });
    function hide(e) {
        $('html').css('overflow', 'auto');
        e.preventDefault();
        $("#search-popup").hide(200);
        $("#search-popup-container").hide(200);
        $("#search-popup").css({ "opacity": "0" });
    }
});
