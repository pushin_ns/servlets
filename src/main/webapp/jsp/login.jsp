<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>

<head>
    <meta charset="utf-8" />
    <title>Логин</title>
    <link rel="stylesheet" type="text/css" href="styles/auth-style.css">
</head>

<body>
    <form method="POST" action="">
        <h1>Авторизация</h1>
        <input type="text" name="login" id="login" placeholder="Логин">
        <input type="password" name="password" id="password" placeholder="Пароль">
        <span id="error"></span>

        <input type="submit" name="submit" id="submit-button" value="Вход">
        <a href="registration">Зарегистрироваться</a>
    </form>
</body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/jquery.color-2.1.2.js"></script>
<script src="js/login-check.js"></script>

</html>